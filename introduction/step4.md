There are some usefull cli options you can use:

## Tagging the image:

Like with the `docker build`, you can specify the name/tag of the final image. The tag can be used instead of the image id. You can specify more of them.

`incubator build -t myimage -t myimage:v2 -t myrepo/myimage:v2`

### Inspect result

Now, we can use each these tags to access the image information:

- Inspect the image:

`docker inspect myimage`

- Get the image layering:

`docker history myimage`

### Run the image

We can try to run our new image: 

`docker run -it --rm myimage bash`

> `-i` allows us to communicate with the image
>
> `-t` allocats the terminal
>
> `--rm` removes the container after exiting the container

### In the container

We can test our environemnt variables:

`env`

When we are satisfied with our testing, we can escape the container with the `exit` command.

## Labels

Labels are some key-value metadata information describing the image. They can be defined using the `-l` or `--label` flag:

`incubator build -t myimage -l key:value`

`docker inspect myimage | grep key`


