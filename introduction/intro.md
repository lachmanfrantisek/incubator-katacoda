[![build status](https://gitlab.com/lachmanfrantisek/incubator/badges/master/build.svg)](https://gitlab.com/lachmanfrantisek/incubator/commits/master)
[![coverage report](https://gitlab.com/lachmanfrantisek/incubator/badges/master/coverage.svg)](https://gitlab.com/lachmanfrantisek/incubator/commits/master)
[![License: MIT](https://img.shields.io/pypi/l/incubator.svg)](https://opensource.org/licenses/MIT)
[![Documentation](https://img.shields.io/badge/documentation-pdoc-green.svg)](https://lachmanfrantisek.gitlab.io/incubator/)
[![PyPI - incubator](https://img.shields.io/pypi/v/incubator.svg)](https://pypi.python.org/pypi/incubator/)
[![PyPI - incubator](https://img.shields.io/pypi/status/incubator.svg)](https://pypi.python.org/pypi/incubator/)
[![PyPI - incubator](https://img.shields.io/pypi/pyversions/incubator.svg)](https://pypi.python.org/pypi/incubator/)

-------

# Incubator

**Python library and command line interface for building container images in better and more secure way.**

With Incubator you can:

* precisely control layering of the image
* mounting build-time volumes (for secrets present only during the build)
* better metadata handling
* use standard *Dockerfile* (extended functionality is defined externally)

This tutorial will teach you, how to do it.

> Documentation can be found at [lachmanfrantisek.gitlab.io/incubator/](https://lachmanfrantisek.gitlab.io/incubator/).
>
> The source is hosted at [gitlab.com/lachmanfrantisek/incubator](https://gitlab.com/lachmanfrantisek/incubator). There you can find the installation methods as well as the issue tracker.
