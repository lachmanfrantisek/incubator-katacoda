I you do not want to write the command-line arguments every time, you can define them in the configuration files.

The files use JSON format and there are two default places:

- home folder: `~/.incubator.rc`
- current working directory: `./.incubator.rc`

If you define one (or both) of these files, *Incubator* will read it automaticly. Another files, you have to connect with the `-g`/`--config` option during the build.

There are various option, that can be setup in this files:

- `version` (number) -- version of schema (now `1`)
- `buildargs` (dict) -- build arguments
- `container_limits` (dict) -- limits for build-time containers
- `context_file_limit` (integer>=0) -- Maximum size of in memory config. (0 is unlimited and default)
- `forcerm` (bool) -- always remove intermediate containers, even after unsuccessful builds
- `infinite_command` -- command used as to run container infinitely (default `sh`)
- `labels` (dict) -- labels to add to the image (visible with `docker inspect image_id_or_name`)
- `layers` (array(integer>=0)) -- array of layer splits (each number `i` sets commit between instruction `i` and `i+1`), empty array means default behaviour and splits commit between each instruction, `[0]` can be used to squash all lyaers
- `mkdir_command` (string) -- command used to create directory (recursively) (default `mkdir --parent`)
- `config_name` (string) -- configuration can have name to better error handling
- `pull` (bool) -- downloads the base image even if it is present
- `rm` (bool) -- remove intermediate containers (default `True`)
- `tags` (array(string)) -- tags to set to the image
- `volumes` (array(string)) -- bind (build-time) volumes, `source:destination` or `source:destination:mode` (modes: `ro`, `rw`, `z` and `Z`)


## Example of merging

As an example, there are created three configuration files:

`~/.incubator.rc`:

```
{
"labels": {
		"author": "myself"
	}
}
```

`./.incubator.rc`:

```
{
"layers" : [3],
"tags" : ["myimage"]
}
```

`./config.json`:

```
{
"tags": ["bestimage"]
}
```

Now, you can try to merge them all:

`incubator build -g config.json`

After the build we can inspect our resulting image:

`docker inspect myimage`

In the beginning, we can se both tags:

```
"RepoTags": [
            "bestimage:latest",
            "myimage:latest"
        ],
```

The label `author` is here as well:

```
"Labels": {
                "author": "myself",
                "layer_0_commands": " FROM fedora:25 RUN echo \"hello\" RUN echo \"world\"",
                "layer_1_commands": " RUN echo \"!\""
            }
```

And in the labels we can also see, that there is a split after the third instruction.
